<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Code extends Model
{
  protected $fillable = [
      'code',
      'slug',
      'uses',
      'reward'
  ];

  public static function createCode(Request $request){
    $_request = $request->all();
    $_request['uses'] = (integer) $_request['uses'];
    $_request['reward'] = (integer) $_request['reward'];
    return Code::create([
        'code' => $_request['code'],
        'slug' => $_request['slug'],
        'uses' => $_request['uses'],
        'reward' => json_encode(['points' => $_request['reward']]),
    ]);
  }

  public static function active(Request $request)
  {
      $_request = $request->all();
      $codigo = DB::table('codes')
      ->where('id',$_request['id'])
      ->first();
      if(!$codigo) return redirect()->back()->with('failure', ['Codigo no encontrado']);
      $codigo = Code::find($codigo->id);
      if(!$codigo) return redirect()->back()->with('failure', ['Error en la base de datos']);
      if ($_request['state'] == true) {
        $codigo->active = true;
        $codigo->save();
        return redirect()->back()->with('sucess', ['Codigo Activado']);
      }else if($_request['state'] == false){
        $codigo->active = false;
        $codigo->save();
        return redirect()->back()->with('sucess', ['Codigo Desactivado']);
      }
  }

  public static function remove(Request $request)
  {
    $_request = $request->all();
    $codigo = DB::table('codes')
    ->where('id',$_request['id'])
    ->first();
    if(!$codigo) return redirect()->back()->with('failure', ['Codigo no encontrado']);
    $codigo = Code::find($codigo->id);
    if(!$codigo) return redirect()->back()->with('failure', ['Error en la base de datos']);
    $codigo->delete();
    return redirect()->back()->with('sucess', ['Codigo Eliminado']);
  }

  public static function getCodes(){
    $query = DB::table('codes');
    return $query->get();
  }
}
