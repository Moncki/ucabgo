<?php namespace  App\Helpers;

  /*
  * Esta clase sirve para subir archvos de una manera mas facil, pero esta mala!
  * depende de: ErrorManager
  */

  use Illuminate\Http\Request;
  use Route;

  class FileManager
  {

    public static function getType($ext){
      $ext = strtolower($ext);
      switch ($ext) {

        case 'png':     return 'image'; break;
        case 'jpg':     return 'image'; break;
        case 'bmp':     return 'image'; break;
        case 'jepg':    return 'image'; break;

        case 'mp3':     return 'audio'; break;
        case 'wav':     return 'audio'; break;

        //Videos que puede reproducir el navegador
        case 'mp4':     return 'video'; break;
        case 'avi':     return 'video'; break;

        //Videos que no puede reproducir el navegador
        case 'mov':     return 'nwvideo'; break;

        //specials
        case 'gif':     return 'gif'; break;
        case 'svg':     return 'svg'; break;

        default:        return 'file'; break;
      }
    }

    public static function isInBlackList($ext){
      $ext = strtolower($ext);
      $blackList = [
        "php"         /* LOGICAMENTE no pueden poner archivos .php como foto de perfil ponte*/,
        "hack"        /* Yolo */,
        "exe"         /* Los viruses siempre son .exe we... */,
        "gitnore"     /*No se que enfermo podria subir esto pero aja*/,
        "pablo"       /*...*/,
      ];
      if (in_array($ext,$blackList))
        return true;

      return false;
    }

    public static function public_route($internal = 0){
      if ($internal)
        return str_replace('public', 'public', public_path().'/upload/');

      return url('/upload/').'/';
    }

    public static function _hasFile(Request $request, $req_name = 'file'){
      return $request->hasFile($req_name);
    }

    public static function saveFile($file, $typesAllowed = null, $size_File = STD_FILE_SIZE){

      $fileIns = new stdClass();

      $proute = FileManager::public_route(1);

      $fileIns->oname = $file->getClientOriginalName();
      $fileIns->size = $file->getSize();

      $fileIns->type = explode('.',$fileIns->oname);
      $fileIns->type = $fileIns->type[sizeof($fileIns->type)-1];

      $fileIns->name = 'file_'.time().'_'.$oname;
      $fileIns->fdir = $proute.$fileIns->name;

      //Comprobe Size of file
      if ($fileIns->size > $size_File) return false; //"El archivo es casi del tamaño de mi pene"

      //Comprobe file is not in blacklist
      if (FileManager::isInBlackList($fileIns->type)) return false;

      //Comprobe type is allowed
      if ($typesAllowed){
        if (!in_array($fileIns->type,$typesAllowed))
        return false;
      }

      //Save file
      if (!$file->move($proute, $fileIns->name)) return false;

      return true;

    }

    public static function store(Request $request, $req_name = 'file', $typesAllowed = null, $iWantOne = 0, $size_File = STD_FILE_SIZE){

      $files = [];

      if (FileManager::hasFile($request, $req_name)){
        $files = $request->file($req_name);
      }else{
        return ErrorManager::error("Archivo no encontrado");
      }

      if (!is_array($files)) $files = [$files];

      if ($iWantOne == 1){
        return FileManager::saveFile($files[0], $typesAllowed, $size_File);
      }

      $_filesSubmit = [];
      foreach ($files as $file) {
        $__ok = FileManager::saveFile($file, $typesAllowed, $size_File);
        if ($__ok) $_filesSubmit[] = $__ok;
        else
        $_filesSubmit[] = null;
      }

      return $_filesSubmit;

    }

    public static function unsetFile($fileRoute = null){
      return ErrorManager::success();
    }

  }
