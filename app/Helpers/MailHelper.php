<?php namespace App\Helpers;

/*
* Esta clase sirve para mandar un email de forma mas facil
*/

class MailHelper {
  public static function send($toEmail, $myEmail, $myName, $title, $message, $state, $NameTo) {

    // mensaje
    $mensaje = '
    <html>
    <head>
      <title>'.$title.'</title>
    </head>
    <body>
      <p>Estado: '.$state.'<br>'.$message.'</p>
    </body>
    </html>
    ';

    // Para enviar un correo HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= 'To: '.$NameTo.' <'.$toEmail.'>' . "\r\n";
    $cabeceras .= 'From: '.$myName.' <'.$myEmail.'>' . "\r\n";
    $cabeceras .= 'Cc: ' .$myEmail. "\r\n";
    $cabeceras .= 'Bcc: ' .$myEmail. "\r\n";

    // Enviarlo
    mail($toEmail, $title, $mensaje, $cabeceras);

  }
}
