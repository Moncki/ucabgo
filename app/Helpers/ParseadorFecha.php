<?php namespace App\Helpers;

  /*
  * Esta clase sirve para formatear cualquier fecha en 'd/m/Y a la hh:ii'
  */

  class ParseadorFecha {
    public static function normalizar($datet = ''){
      $time = strtotime($datet);
      $dYear = date('Y',$time);
      $dMouth = date('m',$time);
      $dday = date('d',$time);
      $dH = date('H',$time);
      $dI = date('i',$time);
      return "$dday/$dMouth/$dYear a la $dH:$dI";
    }
  }

?>
