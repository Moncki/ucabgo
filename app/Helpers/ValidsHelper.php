<?php namespace App\Helpers;

  /*
  * Esta clase sirve para validar algunas cadenas
  */

  class ValidsHelper {
    public static function is_slug($str) {
        return preg_match('/[a-z]{1}[a-z0-9\-]*/i', $str);
    }
  }

?>
