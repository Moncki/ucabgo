<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use Auth;
use App\User;
use App\School;
use App\Code;
use App\CodeUse;
use App\Misions;
use App\Helpers\ResponseHelper;
use QrCode;

class AdminController extends Controller
{

  public function dashboard()
  {
    $codes = Code::getCodes();
    foreach ($codes as $key => $value) {
      $codes[$key]->qrcode = base64_encode(QrCode::format('png')->size(100)->generate($codes[$key]->code));
    }
    $codes = json_encode($codes);
    $misions = Misions::getMisions();
    $misions = json_encode($misions);
    return view('AdminPanel',compact('codes','misions'));
  }

}
