<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use JWTAuth;
use App;
use Mail;
use Exception;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
          'nickname' => ['required', 'string', 'min:4', 'max:16', 'unique:users'],
          'email' => ['required', 'string','min:6', 'email', 'max:180', 'unique:users'],
          'school' => ['required', 'numeric','digits_between:1,20'],
          'phone' => ['string','required','min:4','max:16'],
          'cedula' => ['string','required','min:4','max:12','unique:users'],
          'fullname' => ['required','string','max:32'],
          'password' => ['required','string','max:32', 'min:5', 'confirmed'],
          'gender' => ['required','string','max:4'],
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::createUser($request);
        if (!$user) return response()->json('Error de base de datos', 500);

        //return $created;//201
        $message = [
          'icon'  => '../statics/okhand.png',
          'title' => 'Buenisimo!',
          'text'  => 'Bienvenido a Creemos Online!, ' . $request->input('nickname'),
        ];

        $blacklist = [
          'Matson','matson','matsonwashere','Matsonwashere'
        ];
        if (in_array($request->input('nickname'),$blacklist)) {
          $message = [
            'icon'  => '../statics/wink.png',
            'title' => 'Oye!',
            'text'  => $request->input('nickname') . ', que sorpresa encontrarte.',
          ];
        }

        return response()->json(compact('user','message'),201);

    }

}
