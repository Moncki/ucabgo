<?php

namespace App\Http\Controllers;

use App\Code;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CodeController extends Controller
{
    public function create(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'code' => ['required', 'string', 'min:4', 'max:48', 'unique:codes'],
        'slug' => ['required', 'string','min:4', 'max:24', 'unique:codes'],
        'uses' => ['required', 'numeric','digits_between:1,20'],
        'reward' => ['required', 'numeric', 'digits_between:1,20'],
      ]);
      if($validator->fails()){
          return redirect()->back()->withErrors($validator);
      }
      $created = Code::createCode($request);
      if (!$created) return redirect()->back()->with('failure', ['Error en la base de datos']);

      return redirect()->back()->with('success', ['Codigo creado']);
    }

    public function state(Request $request)
    {
      return Code::active($request);
    }

    public function remove(Request $request)
    {
      return Code::remove($request);
    }

    public function getCodes()
    {
      return Code::getCodes();
    }


      public function QRCode()
      {
        $code = $_REQUEST['code'];
        return view('QRCode',compact('code'));
      }


}
