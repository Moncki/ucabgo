<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use Auth;
use App\User;
use App\School;
use App\Code;
use App\CodeUse;
use App\Misions;
use App\Helpers\ResponseHelper;

class GameController extends Controller
{
    public function exchange(Request $request){

      $user = $request->user();

      //Comprobamos si estas miando fuera del perol
      if ($request->input('code') == null || $request->input('code') == '')
        return response()->json('Ingrese el codigo', 400);

      $code = $request->input('code');

      /*Comprueba si el codigo existe o esta activo*/
      $ExistAndActive = Code::where('code','=',$code)
                        ->where('active','=',1)
                        ->first();

      $message = [
        'icon'  =>  '../statics/sad.png',
        'title' =>  'Lo siento',
        'text'  =>  'Nada por aqui',
      ];

      if (!$ExistAndActive) return response()->json($message);
      /*----*/

      /*Comprueba si el codigo ya lo usaste*/
      $alreadyUse = CodeUse::where('code','=',$ExistAndActive->id)->where('user','=',$user->id)->first();

      $message = [
        'icon'  =>  '../statics/wink.png',
        'title' =>  'Hey!',
        'text'  =>  'Ya usaste este codigo!',
      ];

      if ($alreadyUse) return response()->json($message);
      /*-----*/

      /*Comprueba si el codigo aun se puede usar*/
      if (!($ExistAndActive->uses == -1 || $ExistAndActive->uses > 0)) {
        $message = [
          'icon'  =>  '../statics/sad.png',
          'title' =>  'Lo siento',
          'text'  =>  'Este codigo ha sido usado demaciadas veces',
        ];
        return response()->json($message);
      }
      /*----*/

      if ($ExistAndActive->uses != -1) {
        $ExistAndActive->uses = $ExistAndActive->uses - 1;
      }

      $CodeUse = new CodeUse();
      $CodeUse->user = $user->id;
      $CodeUse->code = $ExistAndActive->id;
      $points = 0;

      /*Latter, Reward Lector Helper.*/
      $reward = json_decode($ExistAndActive->reward);
      foreach ($reward as $key => $value) {
        switch ($key) {
          case 'points':
            $points = $value;
            $user->points = (integer) $user->points + (integer) $value;
            if (!$user->save()) return response()->json('Error interno del servidor', 500);
          break;
        }
      }
      /**/

      if ($CodeUse->save() && $ExistAndActive->save()) {
        $message = [
          'icon'  =>  '../statics/has-obtenido.png',
          'title' =>  '¡Felicidades!',
          'text'  =>  'Has obtenido '.$points.' puntos',
        ];
        return response()->json($message);
      }

      return response()->json('Error interno del servidor', 500);

    }

    public function refresh(Request $request) {

      $user = $request->user();
      $user = User::find($user->id);

      $school = School::find($user->school);
      if (!$school) $school = null;

      $misions = Misions::getMisionsGame();
      $place = User::getMePlace();

      return response()->json([
        'user'    => $user,
        'school'  => $school,
        'events'  => $misions['events'],
        'misions' => $misions['misions'],
        'place'   => $place
      ]);

    }

    public function rank(Request $request) {

      return response()->json(User::getRankingMe());

    }

}
