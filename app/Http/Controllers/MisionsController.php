<?php

namespace App\Http\Controllers;

use App\Misions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MisionsController extends Controller
{
    public function create(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'title' => ['required', 'string', 'min:4', 'max:32'],
        'location' => ['required', 'string', 'min:4', 'max:32'],
        'description' => ['required', 'string','min:4', 'max:512'],
        'type' => ['string','max:20'],
        'date_start' => ['required', 'date'],
        'date_end' => ['required', 'date'],
        'time_start' => ['required', 'date_format:H:i'],
        'time_end' => ['required', 'date_format:H:i'],
        'reward' => ['required', 'numeric'],
      ]);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator);
      }
      $created = Misions::createMision($request);
      if (!$created) return redirect()->back()->with('failure', ['Error en la base de datos']);

      return redirect()->back()->with('success', ['Mision creada']);
    }

    public function remove(Request $request)
    {
      return Misions::remove($request);
    }
    public function state(Request $request)
    {
      return Misions::active($request);
    }

    public function getMisions()
    {
      return Misions::getMisions();
    }


}
