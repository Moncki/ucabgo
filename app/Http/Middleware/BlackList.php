<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use JWTAuth;

use App\User;

class BlackList
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        $user = User::find($user->id);
        if($user)
            if($user->blacklist){
                if(!$request->ajax())
                    return redirect('/');
                else
                    return response()->json('Forbidden',403);
            }
        else
            $user= Auth::user();
            if($user->blacklist){
                if(!$request->ajax())
                    return redirect('/');
                else
                    return response()->json('Forbidden',403);
            }

        return $next($request);
    }
}
