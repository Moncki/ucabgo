<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use JWTAuth;

use App\User;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = -1, $role2 = -1, $role3 = -1, $role4 = -1, $role5 = -1)
    {
        $_user = $request->user();
          if (!$_user) return redirect('/');

        $_user = User::find($_user->id);
          if (!$_user) return redirect('/');

        //var_dump($role4);exit();

        if ( $_user->hasRole($role) || $_user->hasRole($role2) || $_user->hasRole($role3) || $_user->hasRole($role4) || $_user->hasRole($role5) )
        {
          return $next($request);
        }

        if(!$request->ajax())
          return redirect('/');
        else
          return response('Forbidden',403);
    }
}
