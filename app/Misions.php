<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Misions extends Model
{
    protected $fillable = [
        'title',
        'description',
        'date_start',
        'date_end',
        'reward',
        'location'
    ];

    public static function createMision(Request $request){
      $_request = $request->all();
      $_request['reward'] = (integer) $_request['reward'];
      return Misions::create([
          'title' => $_request['title'],
          'location' => $_request['location'],
          'description' => $_request['description'],
          'type' => $_request['type'],
          'date_start' => $_request['date_start'].' '.$_request['time_start'],
          'date_end' => $_request['date_end'].' '.$_request['time_end'],
          'reward' => json_encode(['free' => ['points' => $_request['reward']]]),
      ]);
    }

    public static function active(Request $request)
    {
        $_request = $request->all();
        /*var_dump($_request);exit();*/
        $mision = Misions::find($_request['id']);

        if(!$mision) return redirect()->back()->with('failure', ['Mision no encontrado']);

        $mision->active = (boolean) $_request['state'];
        $mision->save();

        if ($_request['state'])
          return redirect()->back()->with('sucess', ['Mision Activada']);
        else
          return redirect()->back()->with('sucess', ['Mision Desactivada']);

    }

    public static function remove(Request $request)
    {
      $_request = $request->all();
      $mision = DB::table('misions')
      ->where('id',$_request['id'])
      ->first();
      if(!$mision) return redirect()->back()->with('failure', ['Mision no encontrado']);
      $mision = Misions::find($mision->id);
      if(!$mision) return redirect()->back()->with('failure', ['Error en la base de datos']);
      $mision->delete();
      return redirect()->back()->with('sucess', ['Mision Borrada']);
    }

    public static function getMisions(){
      $query = DB::table('misions');
      return $query->get();
    }

    public static function getMisionsGame(){
      $now = Date('Y-m-d H:m:i');

      $events   =  DB::table('misions')
                ->where('date_end','>',$now)
                ->where('active','=',1)
                ->where('type','=','primary')
                ->orderBy('date_start', 'ASC')
                ->get();

      $misions =  DB::table('misions')
                ->where('date_end','>',$now)
                ->where('active','=',1)
                ->where('type','=','secondary')
                ->orderBy('date_start', 'ASC')
                ->get();

      return ['events' => $events,'misions' => $misions];
    }

}
