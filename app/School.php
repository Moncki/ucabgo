<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

class School extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'colorPrimary',
        'colorPrimaryDark',
        'colorSecondary',
        'colorSecondaryDark',
        'urlBoy',
        'urlGirl',
    ];


    public static function getSchools(){

      $query = DB::table('schools');
      return $query->get();

    }

}
