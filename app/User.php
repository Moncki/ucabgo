<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'nickname',
        'email',
        'fullname',
        'cedula',
        'phone',
        'points',
        'inventory',
        'school',
        'type',
        'password',
        'role',
        'blacklist'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static function getUsers($all = false, $blacklist = false){

      $query = DB::table('users');
      if (!$all)
        $query->where("blacklist","=",$blacklist);

      return $query->get();

    }

    public static function getRanking($school = null,$all = false, $blacklist = false){

      $query = DB::table('users');

      if ($school)
        $query->where('school','=',$school);

      if (!$all)
        $query->where("blacklist","=",$blacklist);

      $query->orderBy('points', 'DESC');

      return $query->get();

    }

    public static function getRankingMe(){

      $userId   = Auth::user()->id;
      $myschool = Auth::user()->school;

      $global = json_decode(json_encode(User::getRanking()),1);
      $placeGlobal = null;
      foreach ($global as $place => $player) {
        $global[$place]['place'] = $place+1;
        if ($userId == $player['id']) {
          $placeGlobal = $place+1;
        }
      }

      $school = json_decode(json_encode(User::getRanking($myschool)),1);
      $placeSchool = null;
      foreach ($school as $place => $player) {
        $school[$place]['place'] = $place+1;
        if ($userId == $player['id']) {
          $placeSchool = $place+1;
        }
      }

      return [
        'global' => [
          'ranking' => $global,
          'place' => $placeGlobal
        ],
        'school' => [
          'ranking' => $school,
          'place' => $placeSchool
        ],
      ];

    }

    public static function getMePlace(){

      $userId   = Auth::user()->id;

      $global = User::getRanking();
      $placeGlobal = null;
      foreach ($global as $place => $player) {
        if ($userId == $player->id) {
          $placeGlobal = $place+1;
          break;
        }
      }

      return $placeGlobal;

    }

    public static function createUser(Request $request){
      $_request = $request->all();
      $_request['school'] = (integer) $_request['school'];
      return User::create([
          'nickname' => $_request['nickname'],
          'email' => $_request['email'],
          'school' => $_request['school'],
          'phone' => $_request['phone'],
          'fullname' => $_request['fullname'],
          'cedula' => $_request['cedula'],
          'password' => Hash::make($_request['password']),
          /*'password_confirmation' => $_request['password_confirmation'], <- esto no se guarda bb xD*/
          'gender' => $_request['gender'],
          'role' => 'user',
          'points' => 0,
          'inventory' => '[]',
      ]);
      $token=JWTAuth::fromUser($_request);
      if($_request->save()){
        //Mail::to($user->email)->send(new ConfirmEmailRequest($user));
        try {
            Mail::send('emails.confirmation', ['user' => $_request], function ($m) use ($_request) {
               $m->from('contact@adultapp.com', 'Adult App');

               $m->to($_request->email, $_request->name)->subject(trans('login.confirm_your_email'));
            });
          } catch (Exception $e) {
            /*return error 500*/
            return response(trans('users.mailerror'),500);
          }
        $_request->tokenEmail='';
      }
    }

    public static function banUserWith($with = 'id', $dato = null, $action = true){

      $user = User::where($with,"=",$dato)->first();
      if (!$user) return false;

      $user->blacklist = $action;
      return $user->save();

    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


    public function hasRole($role)
    {
        if ($this->role == $role) {
            return true;
        }
        return false;
    }

}
