<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',32);
            $table->string('colorPrimary');
            $table->string('colorPrimaryDark')->nulleable();
            $table->string('colorSecondary')->nulleable();
            $table->string('colorSecondaryDark')->nulleable();
            $table->text('urlBoy')->nulleable();
            $table->text('urlGirl')->nulleable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
