<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
          •	Nombre Completo
          •	Cedula
          •	Carrera
          •	NickName
          •	Contraseña
        */
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nickname',16) ->unique();
            $table->string('email',180)   ->unique();
            $table->string('phone',16)   ->nulleable();
            $table->string('fullname',32);
            $table->string('cedula',12);
            $table->string('password',180);
            $table->integer('points');
            $table->text('inventory');

            $table->unsignedBigInteger('school');
            $table->foreign('school')->references('id')->on('schools');

            $table->enum('gender',['boy','girl'])->nulleable(); /*¿ Eres chico o chica ?*/
            $table->enum('role',['user','operator','admin','supremegod']);
            $table->boolean('blacklist')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
