<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',32);
            $table->string('location',32);
            $table->string('description',512);
            $table->enum('type',['primary','secondary'])->nulleable();
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->text('reward');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misions');
    }
}
