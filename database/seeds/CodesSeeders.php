<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CodesSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          DB::table('codes')->insert([
             'id' => '4',
             'code' => 'Monckilandia',
             'slug' => 'monckilandia',
             'uses' => '10',
             'active' => '1',
             'reward' => json_encode(['points' => 2])
         ]);

       /*dia 0*/
        DB::table('codes')->insert([
            'id'     => 6,
            'code'   => 'tSS7TvfpY8AriopC',
            'slug'   => 'dia-0-code-0',
            'uses'   => -1,
            'active' => 1,
            'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 7,
           'code'   => 'gpc27pDRoBzeiQfi',
           'slug'   => 'dia-0-code-1',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 8,
           'code'   => 'y72HgZE4LJxgrCxM',
           'slug'   => 'dia-0-code-2',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 9,
           'code'   => 'hdSG9XwnQTVeZx5g',
           'slug'   => 'dia-0-code-3',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 10,
           'code'   => 'nkedANprtxKRYQCm',
           'slug'   => 'dia-0-code-4',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);

        /**/
        DB::table('codes')->insert([
           'id'     => 11,
           'code'   => 'TiQMZCBRZYdNnegZ',
           'slug'   => 'dia-0-code-5',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 12,
           'code'   => 'J92i4G5Zt2UzeohF',
           'slug'   => 'dia-0-code-6',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 13,
           'code'   => 'R8oD4KewVvDC6mAD',
           'slug'   => 'dia-0-code-7',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 14,
           'code'   => 'vQNmYNspzQtxpoCZ',
           'slug'   => 'dia-0-code-8',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 15,
           'code'   => 'KfYBtGUj22sbWVZW',
           'slug'   => 'dia-0-code-9',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);

        /**/
        DB::table('codes')->insert([
           'id'     => 16,
           'code'   => 'pEPrza6UdEwtH5ZW',
           'slug'   => 'dia-0-code-10',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 17,
           'code'   => 'MJac7p76TpWRxcoZ',
           'slug'   => 'dia-0-code-11',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 18,
           'code'   => '96maV93FnoMu66CL',
           'slug'   => 'dia-0-code-12',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 19,
           'code'   => 'G3ZUteS4BTb4XQUB',
           'slug'   => 'dia-0-code-13',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
        ]);
        DB::table('codes')->insert([
           'id'     => 20,
           'code'   => 'wwxZiXRNoRBbPVcA',
           'slug'   => 'dia-0-code-14',
           'uses'   => -1,
           'active' => 1,
           'reward' => json_encode(['points' => 2]),
       ]);

       DB::table('codes')->insert([
          'id'     => 21,
          'code'   => 'cmtbH3qKv4sJ4mYy',
          'slug'   => 'dia-0-code-15',
          'uses'   => -1,
          'active' => 1,
          'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 22,
         'code'   => 'ganq7tzBDRHgfjZG',
         'slug'   => 'dia-0-code-16',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 23,
         'code'   => 'W4eALh9mrrbZmqtp',
         'slug'   => 'dia-0-code-17',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 24,
         'code'   => '33ZuccGpqjM83Wkt',
         'slug'   => 'dia-0-code-18',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 25,
         'code'   => '5iwmqQkvTRYYeXxw',
         'slug'   => 'dia-0-code-19',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 26,
         'code'   => 'bhboEBEkw5owiwsF',
         'slug'   => 'dia-0-code-20',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 27,
         'code'   => '5m9U45EMw2hfZsuS',
         'slug'   => 'dia-0-code-21',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 28,
         'code'   => '4k8MhGSMR8pSi6Kd',
         'slug'   => 'dia-0-code-22',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 29,
         'code'   => 'vM47eLwdNFW5XgNY',
         'slug'   => 'dia-0-code-23',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 30,
         'code'   => 'wGM55Rm9bAXJqAWG',
         'slug'   => 'dia-0-code-24',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 31,
         'code'   => 'BF2w9KM2c7YAXG5v',
         'slug'   => 'dia-0-code-25',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 32,
         'code'   => 'r2EpfAeCEnVVzBbi',
         'slug'   => 'dia-0-code-26',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 33,
         'code'   => 'chqMPHxRkA2UzD2V',
         'slug'   => 'dia-0-code-27',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);
       DB::table('codes')->insert([
         'id'     => 34,
         'code'   => 'sHhCdJYS6XPYhA9H',
         'slug'   => 'dia-0-code-28',
         'uses'   => -1,
         'active' => 1,
         'reward' => json_encode(['points' => 2]),
       ]);


    }
}
