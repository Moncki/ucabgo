<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SchoolsSeeders::class);
        $this->call(UserTableSeeder::class);
        $this->call(ObjectsSeeders::class);
        $this->call(CodesSeeders::class);
        $this->call(MisionsSeeder::class);
    }
}
