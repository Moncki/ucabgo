<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('misions')->insert([
           'id'     => 1,
           'title'   => 'Evento fabuloso',
           'location'   => 'Sitio fabuloso',
           'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
           'type' => 1,
           'date_start' => date('Y-m-17 H:m:s'),
           'date_end' => date('Y-m-17 H:m:s'),
           'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
       ]);
         DB::table('misions')->insert([
            'id'     => 2,
            'title'   => 'Evento fabuloso',
            'location'   => 'Sitio fabuloso',
            'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'type' => 1,
            'date_start' => date('Y-m-17 H:m:s'),
            'date_end' => date('Y-m-17 H:m:s'),
            'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
        ]);
          DB::table('misions')->insert([
             'id'     => 3,
             'title'   => 'Evento fabuloso',
             'location'   => 'Sitio fabuloso',
             'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
             'type' => 1,
             'date_start' => date('Y-m-17 H:m:s'),
             'date_end' => date('Y-m-17 H:m:s'),
             'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
         ]);
         DB::table('misions')->insert([
            'id'     => 4,
            'title'   => 'Evento fabuloso',
            'location'   => 'Sitio fabuloso',
            'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'type' => 1,
            'date_start' => date('Y-m-17 H:m:s'),
            'date_end' => date('Y-m-17 H:m:s'),
            'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
        ]);
        DB::table('misions')->insert([
           'id'     => 5,
           'title'   => 'Evento fabuloso',
           'location'   => 'Sitio fabuloso',
           'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
           'type' => 1,
           'date_start' => date('Y-m-17 H:m:s'),
           'date_end' => date('Y-m-17 H:m:s'),
           'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
       ]);
       DB::table('misions')->insert([
          'id'     => 6,
          'title'   => 'Evento fabuloso',
          'location'   => 'Sitio fabuloso',
          'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'type' => 1,
          'date_start' => date('Y-m-d H:m:s'),
          'date_end' => date('Y-m-d H:m:s'),
          'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
      ]);
      DB::table('misions')->insert([
         'id'     => 7,
         'title'   => 'Evento fabuloso',
         'location'   => 'Sitio fabuloso',
         'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
         'type' => 2,
         'date_start' => date('Y-m-17 H:m:s'),
         'date_end' => date('Y-m-17 H:m:s'),
         'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
     ]);
     DB::table('misions')->insert([
        'id'     => 8,
        'title'   => 'Evento fabuloso',
        'location'   => 'Sitio fabuloso',
        'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'type' => 2,
        'date_start' => date('Y-m-17 H:m:s'),
        'date_end' => date('Y-m-17 H:m:s'),
        'reward' => json_encode(['free' => ['points' => 5], 'win' => ['points' => 25]])
    ]);*/
  }
}
