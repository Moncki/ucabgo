<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ObjectsSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Queda sin usar de momento*/
        DB::table('objects')->insert([
           'id'       => '1',
           'title'    => 'Cofre comun',
           'slug'     => 'comun-chest-25-points',
           'keys'     => 1,
           'type'     => 'chest',
           'reward'   => json_encode([
             'probabilty' => [],
             'forever'    => [
               'points'   => 20
             ]
           ]),
           'urlImage' => 'chest.png',
       ]);

       DB::table('objects')->insert([
          'id'       => '2',
          'title'    => 'Cofre comun',
          'slug'     => 'comun-chest-2-keys',
          'keys'     => 1,
          'type'     => 'chest',
          'reward'   => json_encode([
            'probabilty' => [],
            'forever'    => [
              'keys'   => 2
            ]
          ]),
          'urlImage' => 'chest.png',
      ]);

    }
}
