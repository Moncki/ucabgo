<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolsSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('schools')->insert([
           'id' => '1',
           'title' => 'Ingenieria Informatica',
           'colorPrimary' => '#1f497d',
           'colorPrimaryDark' => '#10253f',
           'colorSecondary' => '#4F81BD',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
       ]);

       DB::table('schools')->insert([
           'id' => '2',
           'title' => 'Ingenieria Civil',
           'colorPrimary' => '#1E6369',
           'colorPrimaryDark' => '#10595B',
           'colorSecondary' => '#64ABAF',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '3',
           'title' => 'Ingenieria Industrial',
           'colorPrimary' => '#7F87AB',
           'colorPrimaryDark' => '#8578A4',
           'colorSecondary' => '#76AFC0',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '4',
           'title' => 'Comunicacion Social',
           'colorPrimary' => '#6F3567',
           'colorPrimaryDark' => '#3B3369',
           'colorSecondary' => '#FC4B56',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '5',
           'title' => 'Derecho',
           'colorPrimary' => '#76150E',
           'colorPrimaryDark' => '#610908',
           'colorSecondary' => '#D63834',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '6',
           'title' => 'Educacion',
           'colorPrimary' => '#44AF9B',
           'colorPrimaryDark' => '#379F9F',
           'colorSecondary' => '#72E790',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '7',
           'title' => 'Relaciones Industriales',
           'colorPrimary' => '#36C5E5',
           'colorPrimaryDark' => '#1DAEF0',
           'colorSecondary' => '#50DAE2',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '8',
           'title' => 'Administracion',
           'colorPrimary' => '#FC6936',
           'colorPrimaryDark' => '#FF4742',
           'colorSecondary' => '#FDA62B',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);
      DB::table('schools')->insert([
           'id' => '9',
           'title' => 'Contaduria',
           'colorPrimary' => '#122F77',
           'colorPrimaryDark' => '#2A2B63',
           'colorSecondary' => '#5D1F3B',
           'colorSecondaryDark' => '#ffffff',
           'urlBoy' => 'hombre-avatar.png',
           'urlGirl' => 'mujer-avatar.png'
      ]);

    }
}
