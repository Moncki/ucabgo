<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           'id' => '1',
           'nickname' => 'Moncki',
           'email' => 'moncki21@gmail.com',
           'phone' => '04249104569',
           'fullname' => 'Eduardo Lara',
           'cedula' => '27935371',
           'password' => Hash::make('27935371a'),
           'points' => '0',
           'inventory' => '[]',
           'school' => '1',
           'gender' => 'boy',
           'role' => '3',
       ]);
       DB::table('users')->insert([
          'id' => '2',
          'nickname' => 'HectorXD',
          'email' => 'Hector1567XD@gmail.com',
          'phone' => '04126992473',
          'fullname' => 'Hector Ferrer',
          'cedula' => '27201276',
          'password' => Hash::make('martha2'),
          'points' => '0',
          'inventory' => '[]',
          'school' => '1',
          'gender' => 'boy',
          'role' => '4',
      ]);
        DB::table('users')->insert([
           'id' => '3',
           'nickname' => 'HectorXD2',
           'email' => 'Hector1567XD2@gmail.com',
           'phone' => '04126992473',
           'fullname' => 'Hector Ferrer',
           'cedula' => '272012762',
           'password' => Hash::make('martha2'),
           'points' => '0',
           'inventory' => '[]',
           'school' => '1',
           'gender' => 'boy',
           'role' => '1',
       ]);
         DB::table('users')->insert([
            'id' => '4',
            'nickname' => 'admin',
            'email' => 'admin@admin.com',
            'phone' => '0000000000',
            'fullname' => 'Administrador',
            'cedula' => '00000000',
            'password' => Hash::make('romanelcrack'),
            'points' => '0',
            'inventory' => '[]',
            'school' => '1',
            'gender' => 'boy',
            'role' => '3',
        ]);
    }
}
