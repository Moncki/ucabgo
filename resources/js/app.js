
require('./bootstrap');

window.Vue = require('vue');

Vue.component('Codigos', require('./components/codigosComponent.vue').default);
Vue.component('Codigo', require('./components/codigoComponent.vue').default);
Vue.component('nuevocodigo', require('./components/nuevoCodigoComponent.vue').default);
Vue.component('nuevamision', require('./components/nuevaMisionComponent.vue').default);
Vue.component('misiones', require('./components/misionesComponent.vue').default);
Vue.component('mision', require('./components/misionComponent.vue').default);


const app = new Vue({
    el: '#app'
});
