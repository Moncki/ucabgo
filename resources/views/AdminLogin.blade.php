@extends('layout')

@section('title','Admin Login')

@section('content')
  <div class="d-flex justify-content-center align-items-center w-100" style="min-height: 100vh;">
    <div class="card text-dark w-50">
      <div class="card-header text-center">
        <h3>Admin Login</h3>
      </div>
      <form action="{{ url('/login') }}" method="post">
        <div class="card-body">
          <div class="md-form input-group mb-3">
            <input type="text" class="form-control" placeholder="NickName" aria-label="NickName" name="username">
            @csrf
          </div>
          <div class="md-form input-group mb-4">
            <input type="password" class="form-control" placeholder="Password" aria-label="Password" name="password">
          </div>
        </div>
        <div class="card-footer d-flex justify-content-end">
          <button type="submit" class="btn btn-primary" >Enviar</button>
        </div>
      </form>
    </div>
  </div>
@endsection
