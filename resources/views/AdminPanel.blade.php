@extends('layout')

@section('title','Admin Panel')

@section('content')
  <div class="d-flex flex-column w-100 text-dark" style="min-height: 100vh; padding: 10px;">
    <div class="d-flex w-100 justify-content-center">
      <h2>CREEMOS ONLINE Admin Panel</h2>
    </div>
    <div class="w-100 d-flex justify-content-around my-3" style="flex-wrap: wrap;">
      <div class="d-flex flex-column col-xs-12 col-12 col-xl-6 col-lg-6 col-md-12">
        <nuevocodigo></nuevocodigo>
        <Codigos :codigos="{{ $codes }}" ></Codigos>
      </div>
      <div class="d-flex flex-column col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12">
        <nuevamision :errores="{{ $errors }}"></nuevamision>
        <misiones :misiones="{{ $misions }}"></misiones>
      </div>
    </div>
  </div>
@endsection
