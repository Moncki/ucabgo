<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') - CREEMOS ONLINE</title>
        <!-- Styles -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
    </head>
    <body>
        <script>
          const baseurl = '{{url("/")}}';
          const __token = document.head.querySelector('meta[name="csrf-token"]');
        </script>
        <div class="padre" id="app">
          @yield('content')
        </div>

        <script src="{{asset('js/jquery-3.4.0.min.js')}}"></script>
        <script src="{{asset('js/popper.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/mdb.min.js')}}"></script>
        <script src="{{asset('js/app.js')}}" charset="utf-8"></script>
    </body>
</html>
