<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['JwtMiddleware']], function () {

  Route::get('/user', function (Request $request) {
      return $request->user();
  });

  Route::get('/game/refresh', 'GameController@refresh');

  Route::get('/game/rank', 'GameController@rank');

  Route::post('/game/code', 'GameController@exchange');

});

Route::group(['middleware' => ['JwtMiddleware','role:operator,admin,supremegod']], function () {

  Route::get('/operator', function (Request $request) {
      return $request->user();
  });

  Route::get('/mision', 'MisionsController@getMisions');

});

Route::group(['middleware' => 'cors'], function() {

  Route::get('/schools', 'SchoolController@todas');

});


Route::post('/users', 'Auth\RegisterController@create');
Route::post('/login', 'Auth\LoginController@login');
