<?php

Route::get('/', function () {
    return view('AdminLogin');
});

Route::group(['middleware' => ['auth','role:operator,admin,supremegod']], function() {

    //Route::get('/admin', function () { return view('AdminPanel'); });
    Route::get('/admin',         'AdminController@dashboard');
    Route::get('/qr',        'CodeController@QRCode');
    Route::post('/code',        'CodeController@create');
    //Route::get('/code',         'CodeController@getCodes'); esta vaina es un peligro x2
    Route::post('/code/state',  'CodeController@state');
    Route::post('/code/remove', 'CodeController@remove');

    Route::post('/mision', 'MisionsController@create');
    Route::post('/mision/state', 'MisionsController@state');
    Route::post('/mision/remove', 'MisionsController@remove');

});

Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');
